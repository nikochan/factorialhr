const puppeteer = require('puppeteer');
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const startUrl = 'https://factorialhr.com/users/sign_in';
const clockInUrl = 'https://app.factorialhr.com/attendance/clock-in/';

const argv = JSON.parse(process.env.npm_config_argv);
const { original: args } = argv;
const [_, __, arg1, arg2] = args;

// date to execute, format: YYYY/M ex: 2019/01
const month = !!arg1 ? arg1 : '';
const closeOnFinish = !!arg2 ;

rl.question("User: ", user => {
  rl.question("Password: ", pass => {
    processBrowser(user, pass);
  });
});

const processBrowser = async (user, password) => {
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();
  page.setViewport({ width: 1024, height: 731, isMobile: false });
  await page.setDefaultNavigationTimeout(0);
  await page.goto(startUrl);

  await page.$eval('input#user_email', (el, user) => el.value = user, user);
  await page.$eval('input#user_password', (el, password) => el.value = password, password);
  await page.$eval('input[type="submit"]', el => el.click());
  await page.waitForNavigation();
  await page.goto(`${clockInUrl}${month}`);
  await page.waitForNavigation({ waitUntil: 'networkidle0' });

  await page.keyboard.press('Space');

  await page.evaluate(() => {
    [].forEach.call(document.querySelectorAll('table svg'), svg => {
      svg.parentNode.click();
    });
  });

  let filledDays = 0;
  const blocks = await page.$$('tbody tr');
  for(let index in blocks){
    const block = blocks[index];
    const className = await block.evaluate(node => node.className);

    if(className.indexOf('disabled') === -1){
      const inputs = await block.$$('input');
      const holiday = await block.$$('div[class^="leaveLabel"]');

      if(inputs.length === 4 && holiday.length === 0){
        const firstValue = await inputs[0].evaluate(node => node.value);

        if(!!firstValue === false){
          filledDays++;

          await inputs[0].focus();
          await page.keyboard.type('09:00');

          await inputs[1].focus();
          await page.keyboard.type('13:30');

          await inputs[2].focus();
          await page.keyboard.type('14:30');

          await inputs[3].focus();
          await page.keyboard.type('18:01');
          await page.keyboard.press('ArrowDown');
        }
      }
    }
  }

  const buttons = await page.$$('div[class^="submit"] button');
  for(let index in buttons){
    await buttons[index].click();
  }

  if(closeOnFinish){
    await browser.close();
  }

  console.log(`\nDONE ✨ (${filledDays} days)`);
};

rl.on("close", function() {
  console.log("\nBye 👋");
  process.exit(0);
});